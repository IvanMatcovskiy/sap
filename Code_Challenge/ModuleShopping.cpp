//C++11
#include <map>
#include <cstring>
#include <cassert>
using namespace std; 

map<int, pair<const char *, const int>> db;

void insert_product(int product_code, const char * name, const int price) {
	db.insert (make_pair(product_code, make_pair(name, price)));
}

void remove_product(int product_code) {
	auto it = db.find(product_code);
	if(it != db.end()) {
		db.erase(it);
	}	
}


int get_product_price(int product_code){
	auto it = db.find(product_code);
	if(it == db.end()) {
		return -1;
	}
	else {
		return it->second.second;
	}
}

const char * get_product_name(int product_code){
	auto it = db.find(product_code);
	if(it == db.end()) {
		return NULL;
	}
	else {
		return it->second.first;
	}
}

int main() {
	assert(get_product_price(42)==-1);
	insert_product(42, "computer", 1000);
	insert_product(42, "duplicate", 2000);
	insert_product(123123, "monitor", 10);
	assert(get_product_price(42)==1000);
	assert(strcmp(get_product_name(42),"computer")==0);
	assert(get_product_price(123123)==10);
	assert(strcmp(get_product_name(123123),"monitor")==0);
	remove_product(42);
	assert(get_product_price(42)==-1);
	assert(get_product_price(123123)==10);

	return 0;
}