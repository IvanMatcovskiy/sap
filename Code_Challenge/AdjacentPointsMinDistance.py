#python3
def solution(A):
    if len(A) == 1:
        return -2
    A.sort()
    min_dist = abs(A[1] - A[0])
    for i in range(2, len(A)):
        dist = abs(A[i] - A[i-1])
        if dist < min_dist:
            min_dist = dist
    if min_dist > 100000000:
        return -1
    
    return min_dist