#python3
def solution(A):
    count_ones = 2
    add = 0
    for i in range(1, len(A)):
        delta = A[i] - A[i-1]
        if delta == 1:
            current = 2 + add
            count_ones += current % 2
            add = current // 2
        elif delta == 2:
            current = 1 + add
            count_ones += current % 2 + (add == 0)
            add = current // 2
        else:
            count_ones += 2
            add = 0
    return count_ones