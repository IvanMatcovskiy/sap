#python3
def solution(A):
    i, j = 0, len(A) - 1
    max_dist = A[i] + A[j] + (j - i)
    while i != j:
        if A[i+1] + A[j] >= A[i] + A[j-1]:
            i += 1
        else:
            j -= 1
        dist = A[i] + A[j] + (j - i)
        if max_dist < dist:
            max_dist = dist
    return max_dist